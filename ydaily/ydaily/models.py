# -*- coding: UTF-8 -*-
import django

__author__ = 'Nick Afanasyev (smilez)'

from django.db import models
from django.utils import  timezone
from datetime import timedelta


class Card(models.Model):
    """
    Cards with context
    """
    name = models.CharField(max_length=32, null=True)
    description = models.CharField(max_length=254, null=True)
    labelId = models.ManyToManyField('Label')
    date = models.DateField(null=False, default=timezone.now)
    colourHEX = models.CharField(max_length=9, default="#ffffff")
    isFinished = models.BooleanField(null=False, default=False)
    isTimerStarted = models.BooleanField(null=False, default=False)
    userId = models.ManyToOneRel('auth.User', to='id', field_name='User')
    startTime = models.DateTimeField(default=timezone.now)
    endTime = models.DateTimeField(default=timezone.now)
    amountOfTime = models.DurationField(default=timedelta(minutes=1))


class Label(models.Model):
    """
    Labels for cards
    """
    name = models.CharField(max_length=32)
    colorHex = models.CharField(max_length=9, default="#ffffff")
